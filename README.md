# Оглавление <!-- omit in toc -->

- [1. Требования](#1-требования)
- [2. Использование](#2-использование)
  - [2.1. Make](#21-make)
  - [2.2. Ручной запуск build.ps1](#22-ручной-запуск-buildps1)
- [Ссылки](#ссылки)

# 1. Требования

1. **Microsoft Word** версии 2010 или выше, с сопутствующими ему COM-объектами в операционной системе.
2. [Pandoc](https://pandoc.org) версии 2.1.1 или выше. Дистрибутив включает также фильтр **pandoc-citeproc**. [Скачать](https://github.com/jgm/pandoc/releases).
3. [pandoc-crossref](https://github.com/lierdakil/pandoc-crossref/releases) — ещё один необходимый фильтр. Внимание: исполняемый файл pandoc-crossref.exe требуется поместить в одну из директорий, указанных в PATH, например в C:\Program Files (x86)\Pandoc (если там находится Pandoc).
4. Microsoft PowerShell. Скрипт `build.ps1` запускает Pandoc, а также совершает постобработку получившегося файла.
5. [Make](https://gnuwin32.sourceforge.net/packages/make.htm). Рекомендуется для автоматизации запуска `build.ps1` и удаления получившихся файлов

# 2. Использование

## 2.1. Make

Для утилиты `make` доступны следующие команды:

- `make docx`. Начать генерацию отчета. Полученный файл будет сохранен по пути `.\build\report.docx`
- `make clean`. Удалить файл `.\build\report.docx`

Также для данных команд в `Makefile` определены переменные:
| Название       | Описание                                                                                                                                                                      |
| -------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| FILE_MD        | Исходный *.md* файл, который будет преобразован в *.docx*                                                                                                                     |
| FILE_DOCX      | Путь, по которому будет сохранен сгенерированный *.docx* файл                                                                                                                 |
| FILE_REFERENCE | Файл, содержащий в себе определение стилей. Используется в `pandoc` в качестве параметра `--reference-doc`. [Подробнее](https://pandoc.org/MANUAL.html#option--reference-doc) |
| FILE_TEMPLATE  | Файл шаблона. Содержит строку `%MAINTEXT%`, которая будет заменена на сгенерированный из *.md* файла текст                                                                    |

## 2.2. Ручной запуск build.ps1

Команда `build.ps1` принимает на вход несколько параметров, некоторые из которых описаны переменными в [предыдущем разделе](#21-make):

| Параметр   | Описание                                                                                                      |
| ---------- | ------------------------------------------------------------------------------------------------------------- |
| md         | FILE_MD                                                                                                       |
| reference  | FILE_REFERENCE                                                                                                |
| docx       | FILE_DOCX                                                                                                     |
| template   | FILE_TEMPLATE                                                                                                 |
| embedfonts | Внедрение шрифтов компании «Паратайп»: [PT Serif, PT Sans и PT Mono](http://rus.paratype.ru/pt-sans-pt-serif) |
| counters   | Посчитать количество элементов в файле (рисунки, таблицы, разделы и т.д.)                                     |

# Ссылки

- [iaaras/gostdown](https://gitlab.iaaras.ru/iaaras/gostdown). Оригинальный проект
- [Pandoc User's Guide](https://pandoc.org/MANUAL.html). Документация по `Pandoc`
- [Word VBA references](https://learn.microsoft.com/en-us/office/vba/api/overview/word). Документация по работе с COM-объектами Word
